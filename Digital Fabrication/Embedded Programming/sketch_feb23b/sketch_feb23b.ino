#include <Adafruit_NeoPixel.h>
#define BUTTON D10
#define NUMPIXELS 1

int previous_state = 1; 
bool flag = true;

int Power = 11;
int PIN  = 12;

int r = 0;
int g = 127;
int b = 255;
 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
 
void setup() {
  pinMode(BUTTON, INPUT_PULLUP);
  pixels.begin();
  pinMode(Power,OUTPUT);
  digitalWrite(Power, HIGH);
  Serial.begin(19200);
}
 
void loop() { 
  // Serial.print("rea: ");
  // Serial.println(digitalRead(BUTTON));
  // Serial.print("flag: ");
  // Serial.println(flag);
  // Serial.print("prevs: ");
  // Serial.println(previous_state);
  

  if(previous_state != digitalRead(BUTTON) && previous_state == 1){
    flag = !flag;
    if(flag==1){
      Serial.println("System back online.");
    }
    else{
      Serial.println("System shut down.");
    }
  }
  previous_state = digitalRead(BUTTON);

  if(flag == true){

    if(Serial.available()){
      String message = Serial.readString();  //read until timeout
      message.trim();
      Serial.print("Received: ");
      Serial.println(message);
    }

    pixels.clear();
    r = (r+3)%255;
    g = (g+2)%255;
    b = (b+1)%255;
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    delay(10);
    pixels.show();

  }


  else{
    if(Serial.available()){
      Serial.readString();
      Serial.println("Standing by, please press the button.");
    }
    pixels.clear();
    //pixels.setPixelColor(0, pixels.Color(0, 0, 0));
    pixels.show();
  }
  
}
